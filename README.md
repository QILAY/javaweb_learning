看第二个视频更改tomcat-user.xml后，登陆manager报警告 An attempt was made to authenticate the locked user,查阅资料后发现是因为server.xml会用到tomcat-user.xml，6.0以后版本的tomcat-user.xml配置改为
<role rolename="tomcat"/>
<role rolename="role1"/>
<user username="tomcat" password="tomcat" roles="tomcat"/>
<user username="both" password="tomcat" roles="tomcat,role1"/>
<user username="role1" password="tomcat" roles="role1"/>
<role rolename="admin-gui"/>
<role rolename="admin-script"/>
<role rolename="manager-gui"/>
<role rolename="manager-script"/>
<role rolename="manager-jmx"/>
<role rolename="manager-status"/>
<user username="user" password="user" roles="manager-gui,manager-script,manager-jmx,manager-status,admin-script,admin-gui"/>
保存重启服务器，就没有再抛警告，配置完后，在localhost/8080的host manager可以用user和user登录管理app
 